const express = require('express');

function createRouter(db) {
  const router = express.Router();
  const user = req.user.email;

  // the routes are defined here

    router.post('/user', (req, res, next) => {
        const user = req.user.email;
    db.query(
        'INSERT INTO User (name, adresse, telephone, matricule, email , password) VALUES (?,?,?,?,?,?)',
        [user, req.body.name, req.body.adresse, req.body.telephone, req.body.matricule.req.body.email, req.body.password],
        (error) => {
        if (error) {
            console.error(error);
            res.status(500).json({status: 'error'});
        } else {
            res.status(200).json({status: 'ok'});
        }
        }
    );
    });

    router.get('/user', function (req, res, next) {
        const user = req.user.email;
        db.query(
          'SELECT id, name, telephone, adresse, matricule, email FROM User WHERE id=? ORDER BY date LIMIT 10 OFFSET ?',
          [id, 10*(req.params.page || 0)],
          (error, results) => {
            if (error) {
              console.log(error);
              res.status(500).json({status: 'error'});
            } else {
              res.status(200).json(results);
            }
          }
        );
    });

    router.put('/user/:id', function (req, res, next) {
        const owner = req.user.email;
        db.query(
          'UPDATE User SET name=?, description=?, date=? WHERE id=? AND owner=?',
          [req.body.name, req.body.description, new Date(req.body.date), req.params.id, owner],
          (error) => {
            if (error) {
              res.status(500).json({status: 'error'});
            } else {
              res.status(200).json({status: 'ok'});
            }
          }
        );
    });

    router.delete('/user/:id', function (req, res, next) {
        const user = req.user.email;
        db.query(
          'DELETE FROM User WHERE id=? AND name=?',
          [req.params.id, name],
          (error) => {
            if (error) {
              res.status(500).json({status: 'error'});
            } else {
              res.status(200).json({status: 'ok'});
            }
          }
        );
      });

      router.delete('/user/:id', function (req, res, next) {
        const owner = req.user.email;
        db.query(
          'DELETE FROM events WHERE id=? AND owner=?',
          [req.params.id, owner],
          (error) => {
            if (error) {
              res.status(500).json({status: 'error'});
            } else {
              res.status(200).json({status: 'ok'});
            }
          }
        );
      });

  return router;
}

module.exports = createRouter;